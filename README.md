legacy-lifegame-javascript
=======================================

JavaScript project for "Refactoring Legacy Code" workshop


SETUP
---------------------------------------

### with docker & docker-compose

```sh
$ docker-compose build
$ docker-compose up
```

entering CLI mode

```sh
$ docker-compose run --rm web bash
```

### local install

```sh
$ npm install
$ npm start
```


DEMO
---------------------------------------

http://localhost:3000/lifegame
