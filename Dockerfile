FROM node:14-buster

WORKDIR /lifegame_javascript

COPY package.json package-lock.json ./
RUN npm install

COPY . ./

CMD ["npm", "start"]
