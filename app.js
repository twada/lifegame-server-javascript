var express = require('express');
var morgan = require('morgan');
var path = require('path');
var app = express();
var port = process.env.PORT || 3000;

app.use(morgan('dev'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/lifegame', function(req, res) {
  if (req.query.prev) {
    var b = [];
    var lns = req.query.prev.split('9');
    for (var i = 0, len = lns.length; i < len; i++) {
      var ln = lns[i];
      var r = [];
      var cs = ln.split('');
      for (var j = 0, len2 = cs.length; j < len2; j++) {
        if (cs[j] == 0) {
          r.push('□');
        } else {
          r.push('■');
        }
      }
      b.push(r);
    }
    var bb = [];
    for (var i = 0; i < 5; i++) {
      bb[i] = [];
      for (var j = 0; j < 5; j++) {
        bb[i][j] = "□";
      }
    }
    for (var i = 0; i < 5; i++) {
      for (var j = 0; j < 5; j++) {
        // 誕生
        if (b[i][j] == "□") {
          var count = 0;
          if (b[i-1] && b[i-1][j-1] == "■") count = count+1;
          if (b[i-1] && b[i-1][j] == "■") count = count+1;
          if (b[i-1] && b[i-1][j+1] == "■") count = count+1;
          if (b[i] && b[i][j-1] == "■") count = count+1;
          if (b[i] && b[i][j+1] == "■") count = count+1;
          if (b[i+1] && b[i+1][j-1] == "■") count = count+1;
          if (b[i+1] && b[i+1][j] == "■") count = count+1;
          if (b[i+1] && b[i+1][j+1] == "■") count = count+1;
          if (count >= 3) {
            bb[i][j] = "■";
          } else {
            bb[i][j] = "□";
          }
        } else {
          bb[i][j] = "□"
        }
        // 生存、過疎、過密を判定
        if (b[i][j] == "■") {
          var count = 0;
          if (b[i-1] && b[i-1][j] == "■") count = count+1;
          if (b[i] && b[i][j-1] == "■") count = count+1;
          if (b[i] && b[i][j+1] == "■") count = count+1;
          if (b[i+1] && b[i+1][j] == "■") count = count+1;
          if (count == 2) {
            bb[i][j] = "■";
          } else {
            bb[i][j] = "□";
          }
        }
      }
    }
    var n = [];
    for (var i = 0; i < 5; i++) {
      var r = bb[i];
      var nr = [];
      for (var j = 0; j < 5; j++) {
        if (r[j] == '□') {
          nr.push('0');
        } else {
          nr.push('1');
        }
      }
      n.push(nr.join(''));
    }
    var tstp = new Date().toISOString();
    res.render('lifegame', { b: bb, s: n.join('9'), tstp: tstp });
  } else {
    var b = [];
    for (var i = 0; i < 5; i++) {
      b[i] = [];
      for (var j = 0; j < 5; j++) {
        b[i][j] = "□";
      }
    }
    b[2][1] = "■";
    b[2][2] = "■";
    b[2][3] = "■";
    var n = [];
    for (var i = 0; i < 5; i++) {
      var r = b[i];
      var nr = [];
      for (var j = 0; j < 5; j++) {
        if (r[j] == '□') {
          nr.push('0');
        } else {
          nr.push('1');
        }
      }
      n.push(nr.join(''));
    }
    var tstp = new Date().toISOString();
    res.render('lifegame', { b: b, s: n.join('9'), tstp: tstp });
  }
});

app.listen(port, function() {
  console.log(`lifegame app listening on port ${port}!`);
});
